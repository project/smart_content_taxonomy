CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


REQUIREMENTS
------------

This module requires the following modules:

 * Smart Content (https://www.drupal.org/project/smart_content)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Configure which taxonomy terms should be used for personalization in
 Structure » Smart Content » Manage Smart Content Taxonomy:

   - Select the taxonomy vocabularies that should be tracked and used to decide
   the personalization experience.

     For the selected vocabularies, the module will record each page that the
     current user visits.

 * View the tracked terms for the current user, grouped by page (node)
 Structure » Smart Content » Manage Smart Content Taxonomy » Terms by page

 * View the tracked terms for the current user, grouped by term Structure »
 Smart Content » Manage Smart Content Taxonomy » Terms by term


TROUBLESHOOTING
---------------

 * To check that the module is tracking data:

   - View the tracked terms for the current user, grouped by page (node)
   Structure » Smart Content » Manage Smart Content Taxonomy » Terms by page

   - View the tracked terms for the current user, grouped by term Structure »
   Smart Content » Manage Smart Content Taxonomy » Terms by term


MAINTAINERS
-----------

Current maintainers:
 * Ashley George - https://www.drupal.org/u/ashley-george

This project has been sponsored by:
 * MICROSERVE
   We're an agency of technical experts, creatives and problem-solvers creating
   ambitious digital experiences. Our expertise helps our clients to transform
   for the better.

   https://microserve.io
