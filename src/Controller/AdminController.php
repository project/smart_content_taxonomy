<?php

namespace Drupal\smart_content_taxonomy\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\node\NodeStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminController.
 *
 * Provides controllers for the 'view tracked terms' pages.
 */
class AdminController extends ControllerBase {

  /**
   * The data from the cookie.
   *
   * @var array
   */
  protected $cookieData;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\taxonomy\TermSorage
   */
  protected $termStorage;

  /**
   * Constructs a new AdminController object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');

    // Decode term data from cookie.
    $this->cookieData = json_decode($_COOKIE['smart_content_taxonomy_data'], TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Controller method for the 'Tracked terms by node' page.
   *
   * @return array
   *   The render array.
   */
  public function trackedTermsByNode() {
    $term_data = [];
    foreach ($this->cookieData as $nid => $node_terms) {
      $node = $this->nodeStorage->load($nid);
      $terms = $this->termStorage->loadMultiple($node_terms);

      // Put the required fields on the variables array.
      $term_data[$nid]['name'] = $node->get('title')->value;
      $term_data[$nid]['bundle'] = $node->bundle();
      $term_data[$nid]['url']  = Url::fromRoute('entity.node.canonical', ['node' => $nid])->toString();
      $term_data[$nid]['terms'] = [];
      foreach ($terms as $term) {
        $term_data[$nid]['terms'][$term->id()]['name'] = $term->get('name')->value;
        $term_data[$nid]['terms'][$term->id()]['vocab'] = $term->get('vid')->target_id;
      }
    }

    return [
      '#theme' => 'tracked_terms_by_node',
        '#term_data' =>$term_data,
    ];
  }

  /**
   * Controller method for the 'Tracked terms by term' page.
   *
   * @return array
   *   The render array.
   */
  public function trackedTermsByTerm() {
    $term_data = [];
    foreach ($this->cookieData as $nid => $node_terms) {
      $terms = $this->termStorage->loadMultiple($node_terms);

      // Put the required fields on the variables array.
      foreach ($terms as $term) {
        $term_data[$term->id()]['name'] = $term->get('name')->value;
        $term_data[$term->id()]['vocab'] = $term->get('vid')->target_id;
        $term_data[$term->id()]['nodes'][$nid] = Url::fromRoute('entity.node.canonical', ['node' => $nid])->toString();
      }
    }
    foreach ($term_data as $id => $term) {
      $term_data[$id]['count'] = count($term_data[$id]['nodes']);

    }
    $count  = array_column($term_data, 'count');
    array_multisort($count, SORT_DESC, $term_data);

    return [
      '#theme' => 'tracked_terms_by_term',
      '#term_data' =>$term_data,
    ];
  }

}
