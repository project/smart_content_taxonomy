<?php

namespace Drupal\smart_content_taxonomy\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Deriver for Taxonomy conditions.
 */
class TaxonomyDerivative extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [
      // The most common term(s) on content that the current user has viewed.
      'top_term' => [
        'label' => 'Top term',
        'type' => 'in_array',
      ] + $base_plugin_definition,
      // All terms on content that the current user has viewed.
      'ever_seen' => [
          'label' => 'Ever seen term',
          'type' => 'in_array',
      ] + $base_plugin_definition,
      // A count of the specified term.
      'term_count' => [
          'label' => 'Term count',
          'type' => 'number_text',
        ] + $base_plugin_definition,
    ];
    return $this->derivatives;
  }

}
