<?php

namespace Drupal\smart_content_taxonomy\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "taxonomy",
 *   label = @Translation("Taxonomy"),
 *   weight = 0,
 * )
 */
class Taxonomy extends ConditionGroupBase {

}
