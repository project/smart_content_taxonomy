<?php

namespace Drupal\smart_content_taxonomy\Plugin\smart_content\Condition\Type;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\smart_content\Condition\Type\ConditionTypeBase;
use Drupal\smart_content\Plugin\smart_content\Condition\Type\Textfield;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'in_array' ConditionType.
 *
 * @SmartConditionType(
 *  id = "in_array",
 *  label = @Translation("In array"),
 * )
 */
class InArray extends ConditionTypeBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config for this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   *
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager, ConfigFactory $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_manager;
    $this->config = $config_factory->get('smart_content_taxonomy.admin');
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['term'] = [
      '#type' => 'select',
      '#title' => $this->t('Select term'),
      '#default_value' => isset($this->configuration['term']) ? $this->configuration['term'] : $this->defaultFieldConfiguration()['term'],
      '#options' => $this->getTermOptions(),
//      '#attributes' => ['class' => ['condition-value']],
//      '#size' => 20,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFieldConfiguration() {
    return [
      'op' => 'equals',
      'term' => '0',
    ];
  }

  /**
   * Returns a list of operators.
   */
  public function getOperators() {
    return [
      'equals' => $this->t('Equals'),
      'starts_with' => $this->t('Starts with'),
      'empty' => $this->t('Is empty'),
    ];
  }

  private function getTermOptions() {
    $vocabs = $this->config->get('personalisation_vocabularies');
    $terms[0] = '- Select term -';

    foreach ($vocabs as $vocab) {
      $vocab_terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocab);
      foreach ($vocab_terms as $term) {
        $terms[$vocab][$term->tid] = $term->name;
      }
    }
    return $terms;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    return ['smart_content_taxonomy/condition_type.in_array'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedSettings() {
    return $this->getConfiguration() + $this->defaultFieldConfiguration();
  }

}
