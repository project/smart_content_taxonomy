<?php

namespace Drupal\smart_content_taxonomy\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartCondition(
 *   id = "taxonomy",
 *   label = @Translation("Taxonomy"),
 *   group = "taxonomy",
 *   weight = 0,
 *   deriver = "Drupal\smart_content_taxonomy\Plugin\Derivative\TaxonomyDerivative"
 * )
 */
class TaxonomyCondition extends ConditionTypeConfigurableBase {

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['smart_content_taxonomy/condition.taxonomy']));
    return $libraries;
  }

}
