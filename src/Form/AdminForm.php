<?php

namespace Drupal\smart_content_taxonomy\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminForm.
 */
class AdminForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $vocabs;

  /**
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $node_types;

  /**
   * AdminForm constructor.
   *
   * @param EntityTypeManagerInterface $entity_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;

    $vocab_storage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
    $this->vocabs = $vocab_storage->loadMultiple($vocab_storage->getQuery()->execute());

    $node_type_storage = $this->entityTypeManager->getStorage('node_type');
    $this->node_types = $node_type_storage->loadMultiple($node_type_storage->getQuery()->execute());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'smart_content_taxonomy.admin',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('smart_content_taxonomy.admin');
    $personalisation_vocabularies = $config->get('personalisation_vocabularies');
    $personalisation_node_types = $config->get('personalisation_node_types');

    $form['node_types'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Select the content types on which tracking
        should be enabled.'),
    ];

    foreach ($this->node_types as $node_type) {
      $form['node_types'][$node_type->id()] = [
        '#type' => 'checkbox',
        '#title' => $node_type->get('name'),
        '#default_value' => in_array($node_type->id(), $personalisation_node_types) ?: FALSE,
      ];
    }

    $form['vocab'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Select the taxonomy vocabularies that should
        be tracked and used to decide the personalization experience.'),
    ];
    foreach ($this->vocabs as $vocab) {
      $form['vocab'][$vocab->id()] = [
        '#type' => 'checkbox',
        '#title' => $vocab->get('name'),
        '#default_value' => in_array($vocab->id(), $personalisation_vocabularies) ?: FALSE,
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('smart_content_taxonomy.admin');
    $personalisation_node_types = [];
    $personalisation_vocabularies = [];

    foreach ($this->node_types as $node_type) {
      if ($form_state->getValue($node_type->id())) {
        $personalisation_node_types[] = $node_type->id();
      }
    }
    foreach ($this->vocabs as $vocab) {
      if ($form_state->getValue($vocab->id())) {
        $personalisation_vocabularies[] = $vocab->id();
      }
    }

    $config->set('personalisation_vocabularies', $personalisation_vocabularies);
    $config->set('personalisation_node_types', $personalisation_node_types);
    $config->save();
  }

}
