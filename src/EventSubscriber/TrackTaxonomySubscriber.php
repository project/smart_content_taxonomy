<?php

namespace Drupal\smart_content_taxonomy\EventSubscriber;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class TrackTaxonomySubscriber.
 */
class TrackTaxonomySubscriber implements EventSubscriberInterface {

  /**
   * The name of the cookie.
   *
   * @var string
   */
  const TAXONOMY_COOKIE = 'smart_content_taxonomy_data';

  /**
   * The tracked data.
   *
   * @var array
   */
  protected $trackedData = [];

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The config for this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new TrackTaxonomySubscriber object.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, ConfigFactory $config_factory) {
    $this->entityManager = $entity_manager;
    $this->config = $config_factory->get('smart_content_taxonomy.admin');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['kernel.request'] = ['kernelRequest'];

    return $events;
  }

  /**
   * This method is called when the kernel.request is dispatched.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   The dispatched event.
   */
  public function kernelRequest(Event $event) {
    // Determine whether the request was for a node.
    $request = $event->getRequest();
    $route_match = RouteMatch::createFromRequest($request);
    $route_name = $route_match->getRouteName();
    if ($route_name == 'entity.node.canonical') {
      /** @var \Symfony\Component\HttpFoundation\ParameterBag $parameters */
      $parameters = $route_match->getParameters();
      foreach ($parameters->getIterator() as $param) {
        // If the parameter is a node, pass to the track method.
        if ($param instanceof NodeInterface) {
          $this->getTerms($param->id());
          break;
        }
      }
    }
  }

  /**
   * Get any terms referenced by a given node.
   *
   * @param $id
   *   The referencing node ID.
   */
  private function getTerms($id) {
    $vocabs = $this->config->get('personalisation_vocabularies');
    $node_types = $this->config->get('personalisation_node_types');

    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->entityManager->getStorage('node')->load($id);

    if (in_array($node->bundle(), $node_types)) {
      $fields = $node->getFields();
      foreach ($fields as $field) {
        if ('entity_reference' == $field->getFieldDefinition()->getType()) {
          $referenced_entities = $node->get($field->getName())->referencedEntities();
          /** @var \Drupal\Core\Entity\Entity $referenced_entity */
          foreach($referenced_entities as $referenced_entity) {
            if ($referenced_entity instanceof Term) {
              $vid = $referenced_entity->getVocabularyId();
              if (in_array($vid, $vocabs)) {
                $this->track($node->id(), $referenced_entity->id());
              }
            }
          }
        }
      }
    }

    $this->dropCookie();
  }

  /**
   * Store the name and nid of a term.
   *
   * @param $nid
   *   The referencing node's ID.
   * @param $name
   *   The term name.
   */
  private function track($nid, $name) {
    $this->trackedData[$nid][] = $name;
  }

  /**
   * Drop a cookie if we've tracked some data.
   */
  private function dropCookie() {
    if (!empty($this->trackedData)) {
      $data = $this->trackedData;
      if (isset($_COOKIE[self::TAXONOMY_COOKIE])) {
        $old_data =  (array) json_decode($_COOKIE[self::TAXONOMY_COOKIE]);
        $data = array_replace($old_data, $data);
      }
      $x = json_encode($data);
      setcookie(self::TAXONOMY_COOKIE, $x , 0, '/');
    }
  }

}
