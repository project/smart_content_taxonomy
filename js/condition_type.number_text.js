/**
 * @file
 * Client-side evaluation for the 'number text' condition type.
 */

(function (Drupal) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.ConditionType = Drupal.smartContent.plugin.ConditionType || {};

  /**
   * The 'number text' condition type can be verified in the same way as a
   * 'number'.
   */
  Drupal.smartContent.plugin.ConditionType['type:number_text'] = Drupal.smartContent.plugin.ConditionType['type:number'];

})(Drupal);
