/**
 * @file
 * Client-side evaluation for the 'in array' condition type.
 */

(function (Drupal) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.ConditionType = Drupal.smartContent.plugin.ConditionType || {};

  /**
   * Callback to verify that the 'in array' condition has been met.
   */
  Drupal.smartContent.plugin.ConditionType['type:in_array'] = function (condition, value) {
    let context = value || '';
    switch (condition.settings['op']) {
      case 'equals':
        return (context != null) && context.includes(condition.settings.term);
    }
    return false;
  };

})(Drupal);
