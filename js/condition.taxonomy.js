/**
 * @file
 * Provides condition values for all tracker conditions.
 */
(function (Drupal) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  /**
   * Get an array of most popular term or terms from the terms tracked.
   */
  Drupal.smartContent.plugin.Field['taxonomy:top_term'] = function (condition) {
    let terms = getTerms();
    let topTerms = [];
    let max = 0;

    for (let item in terms) {
      max = terms[item] > max ? terms[item] : max;
    }
    for (let item in terms) {
      if (terms[item] == max) {
        topTerms.push(item);
      }
    }
    return topTerms;
  };

  /**
   * Get an array of all terms tracked.
   */
  Drupal.smartContent.plugin.Field['taxonomy:ever_seen'] = function (condition) {
    let terms = getTerms();
    let allTerms = [];
    for (let item in terms) {
      allTerms.push(item);
    }
    return allTerms;
  };

  /**
   * Get the count of currently tracked instances of the term specified via
   * the condition settings.
   */
  Drupal.smartContent.plugin.Field['taxonomy:term_count'] = function (condition) {
    let terms = getTerms();
    let term = condition.settings.term;
    if (terms[term] !== undefined) {
      return terms[term];
    }
    return 0;
  }

  /**
   * Get the terms instances from the cookie and count the totals.
   */
  function getTerms() {
    let v;
    let totals = [];

    if (v = document.cookie.match('(^|;) ?' + 'smart_content_taxonomy_data' + '=([^;]*)(;|$)')) {
      let a = JSON.parse(decodeURIComponent(v[2]).replace(/\+/g, " "));
      let b;

      // Count occurrences of each term.
      for (let item in a) {
        b = a[item];
        for (let j = 0; j < b.length; j++) {
          totals[b[j]] = (typeof totals[b[j]] === 'undefined') ? 1 : totals[b[j]] + 1;
        }
      };
    }

    return totals;
  }

})(Drupal);
